import pandas as pd
import requests
from hashlib import sha1
from pymongo import MongoClient
import json
import sqlite3
from datetime import datetime
import os
import warnings
warnings.filterwarnings("ignore")

class Mongo:
    """ Client MongoDb"""
    def __init__(self):
        """Mongo conf"""
        self.client = MongoClient(os.environ['HOST_DB'], 27017)
        self.db = self.client['api_country']

    def close(self):
        """"Close connection"""
        self.client.close()

class ApiCountry(Mongo):
    """
    Object Request Api
    """
    def main(self):
        """"Table builder"""
        print('Wellcome, Table builder \n')
        print('Get Regions \n')
        countries, time = self.get_source_one()
        
        print('Get language of the country \n')
        table = list(map(lambda x: self.get_source_two(x['region'],x['name']), countries))
        table = list(filter(lambda x: x, table))

        print('Building table \n')
        df = pd.DataFrame(table)
        data = df.to_json(orient='records')

        print('Save Data \n')
        self.save_mongo(data, 'countries')
        self.save_sqlite(df, 'db')
        self.save_data(df, 'data')
        print(df, '\n')
        print(df['Time'].describe())

        #Stop System
        while(True):
            pass

    def fetch(self, url, headers):
        """exec request"""
        try:
            response = requests.get(url, headers=headers)
            return response
        except requests.exceptions.RequestException:
            print(f'Not success')

    def get_source_one(self):
        """ Get all request objects"""
        source_one = 'https://restcountries-v1.p.rapidapi.com/all'
        headers = {'x-rapidapi-host': os.environ['HOST_API'],
                    'x-rapidapi-key': os.environ['HOST_API_KEY']}
        response = self.fetch(url=source_one, headers=headers)
        if response.status_code == 200:
            return response.json(), response.elapsed.microseconds
        else:
            return False

    def get_source_two(self, region, country):
        """ Get an object of the request"""
        source_two = 'https://restcountries.eu/rest/v2/name/'
        headers = {}
        path = source_two + country
        response = self.fetch(url=path, headers=headers)
        if response.status_code == 200:
            time = response.elapsed.microseconds / 1000
            response = response.json()
            language = sha1(response[0]['languages'][0]['name'].encode('utf-8')).hexdigest()
            data = {
                'Region': region, 
                'City Name': country, 
                'Lenguage': language,
                'Time': time
                }
            return data
        return False

    def save_mongo(self, data, collection):
        """Save data in mongodb """
        data = json.loads(data)
        self.db.get_collection(collection).insert_many(data)
        self.close()
    
    def save_sqlite(self, df, name):
        """Save data in sqlite3"""
        date = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
        name = f'{name}-{date}.sqlite3'
        db = sqlite3.connect(name)
        df.to_sql('countries', db, if_exists="replace")
    
    def save_data(self, df, name):
        """Save data in json file"""
        name = f'{name}.json'
        df.to_json(name, orient='records')

if __name__ == "__main__":
    ApiCountry().main()