import unittest
from run import ApiCountry, Mongo
import requests
import pymongo
import json
import pandas as pd

class TestCasesMongo(unittest.TestCase):
    def test_init(self):
        mongo = Mongo()
        self.assertIsInstance(mongo.client, pymongo.MongoClient)

class TestCasesCountry(unittest.TestCase):
    def test_fetch(self):
        country = ApiCountry()
        response = country.fetch('https://google.com', {})
        self.assertEqual(200, response.status_code)

    def test_get_source_one(self):
        country = ApiCountry()
        response = country.get_source_one()
        self.assertNotEqual(False, response)

    def test_get_source_two(self):
        country = ApiCountry()
        response = country.get_source_two('Americas','colombia')
        self.assertNotEqual(False, response)
    
    def test_save_mongo(self):
        country = ApiCountry()
        data = json.dumps([{'status':'ok'}])
        status = country.save_mongo(data, 'test')
        self.assertIsNone(status)

    def test_save_sqlite(self):
        country = ApiCountry()
        df = pd.DataFrame([{'a':1, 'b':2}])
        obj = country.save_sqlite(df, 'test')
        self.assertIsNone(obj)

    def test_save_data(self):
        country = ApiCountry()
        df = pd.DataFrame([{'a':1, 'b':2}])
        obj = country.save_data(df, 'test')
        self.assertIsNone(obj)


if __name__ == '__main__':
    print('Run Test ...')
    unittest.main()