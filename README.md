# Country Pandas Project
**Building a pandas board from apis rest**

## Requirements
```
* Docker
* Docker-compose
```

## Installation

```
docker-compose -f local.yml up
```

## Structure Design

![Design](https://apisrun.s3-sa-east-1.amazonaws.com/gitlab/images/Countries+Pandas.png "Structure Design")

**Reference**

[https://www.lucidchart.com/invitations/accept/de9b3279-b782-4f2b-a84f-22484a0438d7](https://www.lucidchart.com/invitations/accept/de9b3279-b782-4f2b-a84f-22484a0438d7)